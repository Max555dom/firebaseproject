
const seconnecter = document.getElementById('signin')
const connexionMail = document.getElementById('connexionMail')
const deconnecter = document.getElementById('signout')
const sinscrire = document.getElementById('sinscire')
const info = document.getElementById('info')

const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

seconnecter.onclick = () =>
    auth.signInWithPopup(provider);

deconnecter.onclick = () =>
    auth.signOut();

sinscrire.onclick = () =>
    createUserWithEmailAndPassword();

connexionMail.onclick = () =>
    auth.signInWithEmailAndPassword( email , password );

auth.onAuthStateChanged(user => {

    if (user) {
        seconnecter.hidden = true;
        deconnecter.hidden = false;
        connexionMail.hidden = true;
        sinscrire.hidden = true;
        // section.hidden = false;
        info.textContent = `Bonjour ${user.displayName} ;
User ID: ${user.uid}`;
    } else {
        seconnecter.hidden = false;
        deconnecter.hidden = true;
        connexionMail.hidden = false;
        sinscrire.hidden = false;
        // section.hidden = true;
        info.textContent = '';

    }
});
